﻿using KartGame.KartSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KartColliderManager : MonoBehaviour
{
	public ArcadeKart arcadeKart;

	private void OnCollisionEnter(Collision collision)
	{
		KartColliderManager collider = collision.gameObject.GetComponent<KartColliderManager>();

		if (collider != null)
		{

			if (collider.arcadeKart.Infected)
			{
				arcadeKart.Infected = true;
				print("Infection passed during collision.");
			}
		}
	}
}
